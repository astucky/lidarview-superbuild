include(lidarview-version)
include(paraview-version)

set(lidarview_extract_dir "${CMAKE_CURRENT_BINARY_DIR}/lidarview/test-extraction")
if (WIN32)
  set(generator "ZIP")
  set(lidarview_exe "${lidarview_extract_dir}/bin/LidarView.exe")
  set(lvpython_exe "${lidarview_extract_dir}/bin/lvpython.exe")
elseif (APPLE)
  set(generator "DragNDrop")
  set(lidarview_appname "LidarView.app")
  set(lidarview_exe "${lidarview_extract_dir}/${lidarview_appname}/Contents/MacOS/LidarView")
  set(lvpython_exe "${lidarview_extract_dir}/${lidarview_appname}/Contents/bin/lvpython")
else ()
  set(generator "TGZ")
  set(lidarview_exe "${lidarview_extract_dir}/bin/LidarView")
  set(lvpython_exe "${lidarview_extract_dir}/bin/lvpython")
endif ()

set(name_suffix "")
if (lidarview_version_branch)
  set(name_suffix "-${lidarview_version_branch}")
  string(REPLACE "/" "" name_suffix ${name_suffix})
endif ()

set(glob_prefix "LidarView${name_suffix}-${lidarview_version_full}*")
superbuild_add_extract_test("lidarview" "${glob_prefix}" "${generator}" "${lidarview_extract_dir}"
  LABELS "LidarView")

function (lidarview_add_test name exe)
  if (NOT exe)
    return ()
  endif ()

  add_test(
    NAME    "lidarview-${name}"
    COMMAND "${exe}"
            ${ARGN})
  set_tests_properties(lidarview-${name}
    PROPERTIES
      LABELS  "LidarView"
      DEPENDS "extract-lidarview-${generator}")
endfunction ()

function (lidarview_add_ui_test name script)
  lidarview_add_test("testui-${name}" "${lidarview_exe}"
    "--dr"
    "--test-directory=${CMAKE_BINARY_DIR}/Testing/Temporary"
    "--test-script=${CMAKE_CURRENT_LIST_DIR}/xml/${script}.xml"
    "--data-directory=${CMAKE_CURRENT_LIST_DIR}/data/"
    ${ARGN}
    "--exit")
endfunction ()

set(python_exception_regex "exception;Traceback;ERR")

function (lidarview_add_python_test name script)
  lidarview_add_test("python-${name}" "${lvpython_exe}"
    "${CMAKE_CURRENT_LIST_DIR}/python/${script}.py"
    "-D" "${CMAKE_CURRENT_LIST_DIR}/data/")
  # check for exceptions and tracebacks during python execution
  if (TEST "lidarview-python-${name}")
    set_tests_properties("lidarview-python-${name}" PROPERTIES
      FAIL_REGULAR_EXPRESSION "${python_exception_regex}"
    )
endif ()
endfunction ()

# Simple test to launch app and test ParaView functionalities.
lidarview_add_ui_test("paraviewui" "TestParaviewUI")

# Simple test to launch app and test LidarView functionalities.
lidarview_add_ui_test("lidarviewui" "TestLidarviewUI"
  "--test-baseline=${CMAKE_CURRENT_LIST_DIR}/baselines/Superbuild-TestLidarViewUI.png")

# Simple test to tests pvpython and lvpython.
lidarview_add_python_test("pvpython" "basic_pvpython")

if (velodynesdk_enabled)
  lidarview_add_python_test("lvpython" "basic_lvpython")
endif ()

if (numpy_enabled)
  lidarview_add_python_test("import-numpy" "import_numpy")
endif ()

if (hesaisdk_enabled)
  lidarview_add_ui_test("lidarviewhesai" "TestLidarviewHesaiSDK"
    "--test-baseline=${CMAKE_CURRENT_LIST_DIR}/baselines/Superbuild-TestLidarviewHesaiSDK.png")
endif ()

if (pdal_enabled)
  lidarview_add_ui_test("lidarviewpdal" "TestLidarviewPDAL"
    "--test-baseline=${CMAKE_CURRENT_LIST_DIR}/baselines/Superbuild-TestLidarviewPDAL.png")
endif ()

if (velodynesdk_enabled AND slam_enabled)
  lidarview_add_ui_test("lidarviewslam" "TestLidarviewSLAM"
    "--test-baseline=${CMAKE_CURRENT_LIST_DIR}/baselines/Superbuild-TestLidarviewSLAM.png")
endif ()
