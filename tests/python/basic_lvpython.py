from lidarview.simple import *
from paraview.vtk.util.misc import vtkGetDataRoot

data_dir = vtkGetDataRoot()

interpreter = GetInterpreterName("Velodyne")
OpenPCAP(f"{data_dir}/cartest.pcap", "VLP-16.xml", interpreter)
