superbuild_add_project(yaml
  LICENSE_FILES
    LICENSE
  CMAKE_ARGS
  -DCMAKE_CXX_STANDARD=11
  -DCMAKE_CXX_STANDARD_REQUIRED=ON
  -DBUILD_TESTING=OFF
  -DYAML_CPP_BUILD_TESTS=OFF
  -DCMAKE_INSTALL_LIBDIR:PATH=lib
  -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
)

superbuild_add_extra_cmake_args(
  -DYAML_DIR:PATH=<INSTALL_DIR>/include/yaml-cpp
)

if (WIN32)
  superbuild_append_flags(cxx_flags "/MD" PROJECT_ONLY)
endif()
