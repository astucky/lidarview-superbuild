# Bundling scripts for LidarView - Apple Specific

# Sanitize check
if(NOT superbuild_python_version)
  message(FATAL_ERROR "superbuild_python_version not set")
endif()

if(NOT SOFTWARE_NAME )
  message(FATAL_ERROR "SOFTWARE_NAME branding not set")
endif()

# the variable lidarview_appname:
# - must be a valid dirname: will be a directory at the top of the .dmg
# - is visible in the macOS GUI when opening the .dmg
# - MUST end with .app (else its tree is not considered as an app by macOS)
set(lidarview_appname "${SOFTWARE_NAME}.app")
set(lidarview_share_folder_path "${lidarview_appname}/Contents/Resources")
set(lidarview_license_path "${lidarview_share_folder_path}/licenses")

set(lidarview_additional_libraries)
if (slam_enabled)
 list(APPEND lidarview_additional_libraries "${superbuild_install_location}/bin/${lidarview_appname}/Contents/Libraries/libLidarSlam.dylib")
endif ()

set(all_plugin_paths)
macro (search_plugin_paths _app_name _plugin_path _installed_plugins)
  foreach (_installed_plugin IN LISTS _installed_plugins)
    if (EXISTS "${superbuild_install_location}/Applications/${lidarview_appname}/Contents/Plugins/lib${_installed_plugin}.dylib")
      list(APPEND all_plugin_paths
        "${superbuild_install_location}/Applications/${lidarview_appname}/Contents/Plugins/lib${_installed_plugin}.dylib")
      continue ()
    endif ()

    foreach (path IN ITEMS "" "${_app_name}" "${_plugin_path}/${_installed_plugin}")
      if (EXISTS "${superbuild_install_location}/${path}/lib${_installed_plugin}.dylib")
        list(APPEND all_plugin_paths
          "${superbuild_install_location}/${path}/lib${_installed_plugin}.dylib")
        break ()
      elseif (EXISTS "${superbuild_install_location}/${path}/${_installed_plugin}.so")
        list(APPEND all_plugin_paths
          "${superbuild_install_location}/${path}/${_installed_plugin}.so")
        break ()
      endif ()
  endforeach ()
endmacro ()

search_plugin_paths("paraview-${paraview_version}" ${paraview_plugin_path} "${paraview_plugins}")
search_plugin_paths("lidarview" ${lidarview_plugin_path} "${lidarview_plugins}")
search_plugin_paths("slam" ${slam_plugin_path} "LidarSlamPlugin")

set(include_regexes)
set(ignore_regexes)

superbuild_apple_create_app(
  "\${CMAKE_INSTALL_PREFIX}"
  "${lidarview_appname}"
  "${superbuild_install_location}/Applications/${lidarview_appname}/Contents/MacOS/${SOFTWARE_NAME}"
  CLEAN
  PLUGINS ${all_plugin_paths}
  SEARCH_DIRECTORIES
    "${superbuild_install_location}/bin/${lidarview_appname}/Contents/Libraries" #WIP probably not the best idea to install them here
    "${superbuild_install_location}/lib"
  ADDITIONAL_LIBRARIES ${lidarview_additional_libraries}
  INCLUDE_REGEXES     ${include_regexes}
  IGNORE_REGEXES      ${ignore_regexes})

function (lidarview_install_xml _plugin_path)
  file(GLOB _xml_files "${_plugin_path}/*.xml")
  install(
    FILES       ${_xml_files}
    DESTINATION "${lidarview_appname}/Contents/Plugins"
    COMPONENT   superbuild)
endfunction ()

lidarview_install_xml(${lidarview_plugin_path})
lidarview_install_xml(${paraview_plugin_path})

if (slam_enabled)
  lidarview_install_xml(${slam_plugin_path})
endif ()

# Install Configuration file
if (EXISTS "${superbuild_install_location}/Applications/${SOFTWARE_NAME}.app/Contents/Resources/${SOFTWARE_NAME}.conf")
  file(READ "${superbuild_install_location}/Applications/${SOFTWARE_NAME}.app/Contents/Resources/${SOFTWARE_NAME}.conf" conf_contents)
  string(REGEX REPLACE "[^\n]*/" "../Plugins/" pkg_conf_contents "${conf_contents}")
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${SOFTWARE_NAME}.conf" "${pkg_conf_contents}")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/${SOFTWARE_NAME}.conf"
    DESTINATION "${lidarview_appname}/Contents/Resources/"
    COMPONENT   superbuild)
endif ()

# Bundle Icon
install(
  FILES       "${superbuild_install_location}/Applications/${SOFTWARE_NAME}.app/Contents/Resources/logo.icns"
  DESTINATION "${lidarview_appname}/Contents/Resources"
  COMPONENT   superbuild)

# Info.plist
install(
  FILES       "${superbuild_install_location}/Applications/${SOFTWARE_NAME}.app/Contents/Info.plist"
  DESTINATION "${lidarview_appname}/Contents"
  COMPONENT   superbuild)

# Remove "LidarView" from the list since we just installed it above.
list(REMOVE_ITEM lidarview_executables
  ${SOFTWARE_NAME})

# Install Executables
# WIP WE DISABLE THIS FOR NOW
#foreach (executable IN LISTS lidarview_executables)
#  superbuild_apple_install_utility(
#    "\${CMAKE_INSTALL_PREFIX}"  #DEST
#    "${lidarview_appname}" # Same name as application creation
#    "${superbuild_install_location}/bin/${executable}"
#    SEARCH_DIRECTORIES "${library_paths}")
#endforeach ()

if (qt5_enabled)
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/qt.conf" "[Paths]\nPlugins = Plugins\n")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/qt.conf"
    DESTINATION "${lidarview_appname}/Contents/Resources" #WIP PV DOES THIS BUT LV HAS Application/ ? WIP
    COMPONENT   superbuild)
endif ()

if (python3_enabled)
  # install python modules
  if (python3_built_by_superbuild)
    include(python3.functions)
    superbuild_install_superbuild_python3(BUNDLE "${lidarview_appname}")
  endif()

 file(GLOB egg_dirs
    "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/*.egg/")
  superbuild_apple_install_python(
  "\${CMAKE_INSTALL_PREFIX}"
  "${lidarview_appname}"
  MODULES ${python_modules}
  MODULE_DIRECTORIES
          "${superbuild_install_location}/Applications/paraview.app/Contents/Python"
          "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
          ${egg_dirs}
          "${superbuild_install_location}/Applications/${lidarview_appname}/Contents/Python"
  SEARCH_DIRECTORIES
          "${superbuild_install_location}/Applications/paraview.app/Contents/Libraries"
          "${superbuild_install_location}/lib"
          "${superbuild_install_location}/Applications/${lidarview_appname}/Contents/Libraries"
  )
endif ()

# Configure CMakeDMGSetup.scpt to replace the app name in the script.
# THIS REQUIRES `lidarview_appname`
configure_file(
  "${CMAKE_CURRENT_LIST_DIR}/files/CMakeDMGSetup.scpt.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CMakeDMGSetup.scpt"
  @ONLY)

#WIP TO CHECK
#[[
# For some reason these .so files are not processed by the command
# superbuild_apple_install_python above, so we have to specify them manually
# it could be that I failed to find the correct name(s) to add in parameter
# "MODULE" but I do not think so because there are 86 such files, and because
# they seem to be part of vtk which is already specified like that in ParaView
file(GLOB missing_python_so "${superbuild_install_location}/bin/${lidarview_appname}/Contents/Libraries/vtk*Python.so")
foreach (python_so ${missing_python_so})
  superbuild_apple_install_module(
    "\${CMAKE_INSTALL_PREFIX}"
    "${lidarview_appname}"
    "${python_so}"
    "Contents/Libraries") # destination path inside bundle
endforeach()

# My understanding is that these module are not processed automatically
# by superbuild_apple_create_app because there is no path leading to
# them in binary LidarView or in any of its .dylib dependencies
foreach (module ${lidarview_modules})
  superbuild_apple_install_module(
    "\${CMAKE_INSTALL_PREFIX}"
    "${lidarview_appname}"
    "${superbuild_install_location}/bin/${lidarview_appname}/Contents/Libraries/${module}"
    "Contents/Libraries") # destination path inside bundle
endforeach()

]]

set(CPACK_DMG_BACKGROUND_IMAGE      "${CMAKE_CURRENT_LIST_DIR}/files/CMakeDMGBackground.tif")
set(CPACK_DMG_DS_STORE_SETUP_SCRIPT "${CMAKE_CURRENT_BINARY_DIR}/CMakeDMGSetup.scpt")

message(STATUS "qt5_plugin_paths is ${qt5_plugin_paths}")
foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_apple_install_module(
    "\${CMAKE_INSTALL_PREFIX}"
    "${lidarview_appname}"
    "${qt5_plugin_path}"
    "Contents/Plugins/${qt5_plugin_group}"
    SEARCH_DIRECTORIES  "${superbuild_install_location}/lib"
    INCLUDE_REGEXES     ${include_regexes}
    IGNORE_REGEXES      ${ignore_regexes})
endforeach ()

lidarview_install_extra_data()
