set(LIDAVIEW_EXTRA_CMAKE_ARGUMENTS ""
  CACHE STRING "Extra arguments to be passed to LidarView when configuring.")
mark_as_advanced(LIDAVIEW_EXTRA_CMAKE_ARGUMENTS)

set(paraview_binary_dir)
if (TARGET paraview)
  set(paraview_binary_dir "<BINARY_DIR>")
  _ep_replace_location_tags(paraview paraview_binary_dir)
endif ()

superbuild_add_project(lidarview
  DEPENDS cxx11 paraview qt5 pcap boost eigen yaml python3 numpy
  DEPENDS_OPTIONAL slam hesaisdk velodynesdk pcl ceres opencv nanoflann pdal
  DEBUGGABLE
  DEFAULT_ON
  LICENSE_FILES
    LICENSE

  CMAKE_ARGS
    # LidarView base configuration
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DCMAKE_MACOSX_RPATH:BOOL=OFF
    -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
    -DCMAKE_INSTALL_LIBDIR:PATH=lib

    # LidarView dependencies options
    -DLIDARVIEW_USE_CERES:BOOL=${ceres_enabled}
    -DLIDARVIEW_USE_OPENCV:BOOL=${opencv_enabled}
    -DLIDARVIEW_USE_NANOFLANN:BOOL=${nanoflann_enabled}
    -DLIDARVIEW_USE_PDAL:BOOL=${pdal_enabled}
    -DLIDARVIEW_USE_PCL:BOOL=${pcl_enabled}
    -DLIDARVIEW_USE_PCAP:BOOL=${pcap_enabled}

    # LidarView features options
    -DPARAVIEW_PLUGIN_ENABLE_VelodynePlugin:BOOL=${velodynesdk_enabled}
    -DPARAVIEW_PLUGIN_ENABLE_HesaiPlugin:BOOL=${hesaisdk_enabled}

    -DPCL_PLUGIN_EXPERIMENTAL_SURFACE_FILTERS:BOOL=ON

    # External plugins options
    -DLIDARVIEW_USE_LIDARSLAM_PLUGIN=${slam_enabled}

    ${LIDAVIEW_EXTRA_CMAKE_ARGUMENTS}
)

# This Disable Boost autolinking feature and ease use of Boost as a dynamic library.
# Boost_USE_STATIC_LIBS is off by default, but sometimes that is not sufficient
# on windows (especially with MSVC ?)
if (WIN32 OR APPLE)
  superbuild_append_flags(cxx_flags "-DBOOST_ALL_NO_LIB" PROJECT_ONLY)
  superbuild_append_flags(cxx_flags "-DBOOST_ALL_DYN_LINK" PROJECT_ONLY)
endif()

# On windows we want to have python and qt dll in binary to be able to execute LidarView
if (WIN32)
  superbuild_project_add_step(lidarview-copylibs
    COMMAND "${CMAKE_COMMAND}"
            "-DQt5_DIR:PATH=${Qt5_DIR}"
            "-DUSE_SYSTEM_python3:BOOL=${USE_SYSTEM_python3}"
            "-Dsuperbuild_python_version:STRING=${superbuild_python_version}"
            "-Dinstall_location:PATH=<INSTALL_DIR>"
            -P "${CMAKE_CURRENT_LIST_DIR}/scripts/lidarview.copylibs.cmake"
    DEPENDEES install
    COMMENT "Copy external .dll files to the bin/ directory"
    WORKING_DIRECTORY <SOURCE_DIR>)
endif ()

