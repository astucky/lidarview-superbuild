# Bundling scripts for LidarView - Unix Specific

# Sanitize check
if(NOT superbuild_python_version)
  message(FATAL_ERROR "superbuild_python_version not set")
endif()

if(NOT SOFTWARE_NAME )
  message(FATAL_ERROR "SOFTWARE_NAME branding not set")
endif()

set(lidarview_share_folder_path "share")
set(lidarview_license_path "${lidarview_share_folder_path}/licenses")

set(library_paths "${superbuild_install_location}/lib")
if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../..")
endif ()

set(include_regexes)
set(exclude_regexes)

# Install Executables
foreach (executable IN LISTS lidarview_executables)
  superbuild_unix_install_program(
    "${superbuild_install_location}/bin/${executable}"
    "lib"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES     ${include_regexes} ".*libz\.so\.1\.2\.11.*" # LIBZ dependency patch
    EXCLUDE_REGEXES     ${exclude_regexes} ".*libz.*" # LIBZ dependency patch
  )
endforeach ()

# Install Configuration file
if (EXISTS "${superbuild_install_location}/bin/${SOFTWARE_NAME}.conf")
  install(
    FILES       "${superbuild_install_location}/bin/${SOFTWARE_NAME}.conf"
    DESTINATION "bin"
    COMPONENT   superbuild)
endif ()

# Install Python
if (python3_enabled)
  file(GLOB egg_dirs
    "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/*.egg/")
  if (python3_built_by_superbuild)
    include(python3.functions)
    superbuild_install_superbuild_python3(
      LIBSUFFIX "/python${superbuild_python_version}")
  endif ()

  # Add extra paths to MODULE_DIRECTORIES here (.../local/lib/python${superbuild_python_version}/dist-packages)
  # is a workaround to an issue when building against system python.  When we move to
  # Python3, we should make sure all the python modules get installed to the same
  # location to begin with.
  #
  # Related issue: https://gitlab.kitware.com/paraview/paraview-superbuild/-/issues/120
  superbuild_unix_install_python(
    LIBDIR              "lib"
    MODULES             ${python_modules}
    INCLUDE_REGEXES     ${include_regexes}
    EXCLUDE_REGEXES     ${exclude_regexes}
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
                        ${egg_dirs}
    LOADER_PATHS        "${library_paths}")
endif ()

# THIRDPARTY

# Install Qt Plugins if any
foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  # Qt expects its libraries to be in `lib/`, not beside, so install them as
  # modules.
  superbuild_unix_install_module("${qt5_plugin_path}"
    "lib"
    "plugins/${qt5_plugin_group}/"
    LOADER_PATHS    "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

if (qt5_enabled AND qt5_plugin_paths)
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/qt.conf" "[Paths]\nPrefix = ..\n")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/qt.conf"
    DESTINATION "bin"
    COMPONENT   superbuild)
endif ()

function (lidarview_install_plugins _plugin_path _installed_plugins)
  # Install plugins .so
  foreach (_installed_plugin IN LISTS _installed_plugins)
    superbuild_unix_install_plugin("${_installed_plugin}.so"
      "lib"
      "${_plugin_path}/${_installed_plugin}/"
      LOADER_PATHS    "${library_paths}" # Means "Like SEARCH_DIRECTORIES, but also used to replace RPATHS in _resolve_rpath()"
      INCLUDE_REGEXES ${include_regexes}
      EXCLUDE_REGEXES ${exclude_regexes}
      LOCATION        "${_plugin_path}/${_installed_plugin}/")
  endforeach ()

  # Install xmls
  file(GLOB _xml_files "${superbuild_install_location}/${_plugin_path}/*.xml")
  install(
    FILES       ${_xml_files}
    DESTINATION "${_plugin_path}"
    COMPONENT   superbuild)
endfunction ()

# Install Lidarview Plugins
# see discussion on paraview/paraview-superbuild!865 for why this delayed
# until the end.
lidarview_install_plugins(${lidarview_plugin_path} "${lidarview_plugins}")
lidarview_install_plugins(${paraview_plugin_path} "${paraview_plugins}")

if (slam_enabled)
  lidarview_install_plugins(${slam_plugin_path} "LidarSlamPlugin")
endif ()

lidarview_install_extra_data()
