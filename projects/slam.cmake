set(paraview_binary_dir)
if (TARGET paraview)
  set(paraview_binary_dir "<BINARY_DIR>")
  _ep_replace_location_tags(paraview paraview_binary_dir)
endif ()

superbuild_add_project(slam
  DEPENDS eigen ceres pcl nanoflann boost
  DEPENDS_OPTIONAL paraview g2o openmp opencv gtsam teaserpp qt5 python3
  DEBUGGABLE
  LICENSE_FILES
    LICENSE.txt

  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_OpenCV=${opencv_enabled}
    -DENABLE_g2o=${g2o_enabled}
    -DENABLE_GTSAM=${gtsam_enabled}
    -DENABLE_teaserpp=${teaserpp_enabled}
    -DSLAM_PARAVIEW_PLUGIN:BOOL=${paraview_enabled}
    -DCMAKE_INSTALL_LIBDIR:PATH=lib
)

if (WIN32)
  superbuild_append_flags(cxx_flags "/MD" PROJECT_ONLY)
endif()
