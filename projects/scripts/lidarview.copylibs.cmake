# On windows, python and qt dll must me next to executable to be run

if (NOT superbuild_python_version OR NOT Qt5_DIR OR NOT install_location)
  message(FATAL_ERROR "Cannot copy lidarview runtime external libraries.")
endif ()

# Ship Qt5
foreach (qt5_opengl_lib IN ITEMS opengl32sw libEGL libGLESv2 libEGLd
  Qt5Core Qt5Gui Qt5Help Qt5PrintSupport Qt5Sql Qt5Svg Qt5Widgets Qt5Network)
  file(
    COPY "${Qt5_DIR}/../../../bin/${qt5_opengl_lib}.dll"
    DESTINATION "${install_location}/bin"
  )
endforeach ()

foreach (qt5_plugin_dir IN ITEMS platforms styles iconengines imageformats)
  file(
    COPY "${Qt5_DIR}/../../../plugins/${qt5_plugin_dir}"
    DESTINATION "${install_location}/bin"
  )
endforeach ()

# Ship Python (if not system python)
if (NOT ${USE_SYSTEM_python3})
  set(python_lib_location "${install_location}/Python")
  string(REPLACE "." "" python_version_suffix ${superbuild_python_version})
  file(
    COPY "${python_lib_location}/python${python_version_suffix}.dll"
    DESTINATION "${install_location}/bin"
  )
  file(
    COPY "${python_lib_location}/Lib"
    DESTINATION "${install_location}/bin"
  )
endif ()
