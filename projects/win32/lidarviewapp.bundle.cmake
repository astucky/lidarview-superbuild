# Bundling scripts for LidarView - Win32 Specific
include(lidarview-version)

# Sanitize check
if(NOT SOFTWARE_NAME)
  message(FATAL_ERROR "SOFTWARE_NAME not set")
endif()
if(NOT superbuild_python_version)
  message(FATAL_ERROR "superbuild_python_version not set")
endif()
if(NOT lidarview_version_full)
  message(FATAL_ERROR "lidarview_version_full not set")
endif()
if(NOT SOFTWARE_ICON_PATH)
  message(FATAL_ERROR "SOFTWARE_ICON_PATH not set")
endif()

set(lidarview_share_folder_path "share")
set(lidarview_license_path "${lidarview_share_folder_path}/licenses")

# Set NSIS install specific stuff.
if (CMAKE_CL_64)
  # Change default installation root path for Windows x64.
  set(CPACK_NSIS_INSTALL_ROOT "$PROGRAMFILES64")
endif ()

set(CPACK_WIX_UPGRADE_GUID "bee4ce6c-446f-47ab-a94c-888493562b2d")
set(CPACK_WIX_PRODUCT_GUID "4b00d9a7-e620-4983-8bff-d645c7c3d1b2")

if (NOT "$ENV{GITLAB_CI}" STREQUAL "")
  # Suppress validation.  It does not work without
  # an interactive session or an admin account.
  # https://github.com/wixtoolset/issues/issues/3968
  list(APPEND CPACK_WIX_LIGHT_EXTRA_FLAGS "-sval")
endif ()

set(CPACK_NSIS_HELP_LINK "https://lidarview.kitware.com/")
set(${SOFTWARE_NAME}_description "${SOFTWARE_NAME} ${lidarview_version_full}")
set(CPACK_NSIS_MUI_ICON "${SOFTWARE_ICON_PATH}")

set(library_paths "${superbuild_install_location}/bin")
if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../../../bin")
endif ()

set(include_regexes)
set(exclude_regexes)
if (python3_enabled)
  if (python3_built_by_superbuild)
    list(APPEND library_paths
      "${superbuild_install_location}/Python")
  else()
    list(APPEND exclude_regexes
        ".*python3[0-9]+.dll")
  endif()
endif ()

# Install Executables
foreach (executable IN LISTS lidarview_executables)
  if (DEFINED "${executable}_description")
    list(APPEND CPACK_NSIS_MENU_LINKS
      "bin/${executable}.exe" "${${executable}_description}")
  endif ()

  superbuild_windows_install_program(
  "${executable}" #NOTE: This macro gives no choice in executable origin location, must be in /install/bin
  "bin"
    SEARCH_DIRECTORIES "${library_paths}"
    INCLUDE_REGEXES     ${include_regexes}
    EXCLUDE_REGEXES     ${exclude_regexes}
  )
endforeach()

# Install additional modules #if ever needed use
# foreach _superbuild_windows_install_binary( ... ) with ${lidarview_modules} with DESTINATION "bin"

function (lidarview_install_plugins _plugin_path _installed_plugins)
  # Install plugins .dll
  foreach (_installed_plugin IN LISTS _installed_plugins)
    superbuild_windows_install_plugin("${_installed_plugin}.dll"
      "bin"
      "${_plugin_path}/${_installed_plugin}/"
      SEARCH_DIRECTORIES "${library_paths}" # Same as LOADER_PATHS on UNIX
      INCLUDE_REGEXES    ${include_regexes}
      EXCLUDE_REGEXES    ${exclude_regexes}
      LOCATION           "${_plugin_path}/${_installed_plugin}/")
  endforeach ()

  # Install xmls
  file(GLOB _xml_files "${superbuild_install_location}/${_plugin_path}/*.xml")
  install(
    FILES       ${_xml_files}
    DESTINATION "${_plugin_path}"
    COMPONENT   superbuild)
endfunction ()

lidarview_install_plugins(${lidarview_plugin_path} "${lidarview_plugins}")
lidarview_install_plugins(${paraview_plugin_path} "${paraview_plugins}")

if (slam_enabled)
  lidarview_install_plugins(${slam_plugin_path} "LidarSlamPlugin")
endif ()

# Install Configuration file
if (EXISTS "${superbuild_install_location}/bin/${SOFTWARE_NAME}.conf")
  install(
    FILES       "${superbuild_install_location}/bin/${SOFTWARE_NAME}.conf"
    DESTINATION "bin"
    COMPONENT   superbuild)
endif ()

if (python3_enabled)
  if (python3_built_by_superbuild)
    include(python3.functions)
    superbuild_install_superbuild_python3()
  endif ()

  superbuild_windows_install_python(
    MODULES ${python_modules}
    MODULE_DIRECTORIES  "${superbuild_install_location}/Python/Lib/site-packages"
                        "${superbuild_install_location}/bin/Lib/site-packages"
                        "${superbuild_install_location}/lib/site-packages"
                        "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
                        "${superbuild_install_location}/lib/paraview-${paraview_version_major}.${paraview_version_minor}/site-packages"
    SEARCH_DIRECTORIES  "${superbuild_install_location}/lib"
                        "${superbuild_install_location}/bin"
                        "${superbuild_install_location}/Python"
                        "${superbuild_install_location}/Python/Lib/site-packages/pywin32_system32"
                        ${library_paths}
    EXCLUDE_REGEXES     ${exclude_regexes})

  if (pywin32_built_by_superbuild)
      install(
        DIRECTORY   "${superbuild_install_location}/Python/Lib/site-packages/win32"
        DESTINATION "bin/Lib/site-packages"
        COMPONENT   superbuild)
      install(
        DIRECTORY   "${superbuild_install_location}/Python/Lib/site-packages/pywin32_system32"
        DESTINATION "bin/Lib/site-packages"
        COMPONENT   superbuild)
      install(
        FILES       "${superbuild_install_location}/Python/Lib/site-packages/pywin32.pth"
                    "${superbuild_install_location}/Python/Lib/site-packages/pywin32.version.txt"
        DESTINATION "bin/Lib/site-packages"
        COMPONENT   superbuild)
  endif ()
endif ()

# THIRDPARTY

# Install Qt Plugins if any
foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_windows_install_plugin(
    "${qt5_plugin_path}"
    "bin"
    "bin/${qt5_plugin_group}"
    SEARCH_DIRECTORIES "${library_paths}")
endforeach ()

if (qt5_enabled)
  foreach (qt5_opengl_lib IN ITEMS opengl32sw libEGL libGLESv2)
    superbuild_windows_install_plugin(
      "${Qt5_DIR}/../../../bin/${qt5_opengl_lib}.dll"
      "bin"
      "bin"
      SEARCH_DIRECTORIES "${library_paths}")
  endforeach ()
endif ()

lidarview_install_extra_data()
