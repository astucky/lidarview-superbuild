include(paraview-version)

set(paraview_plugin_path "bin/paraview-${paraview_version}/plugins")
set(lidarview_plugin_path "bin/lidarview/plugins")
set(slam_plugin_path "bin/slam/plugins")

# Trigger lidarview common bundling
include(lidarview.bundle.common)

# Trigger common lidarview based app bundling
include(lidarviewapp.bundle)
