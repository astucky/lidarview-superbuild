if(CMAKE_CL_64)
  set(wpcap_library_dir <BINARY_DIR>/Lib/x64)
else()
  set(wpcap_library_dir <BINARY_DIR>/Lib)
endif()

# Simply use WinPcap development files from their webside.
superbuild_add_project(pcap
  BUILD_IN_SOURCE 1
  LICENSE_FILES
    WpcapSrc_4_1_3/wpcap/libpcap/LICENSE
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ${CMAKE_COMMAND}
    -DPCAP_SHARED_LIBRARY:FILEPATH=${wpcap_library_dir}/wpcap.dll
    -DPCAP_STATIC_LIBRARY:FILEPATH=${wpcap_library_dir}/wpcap.lib
    -DPACKET_SHARED_LIBRARY:FILEPATH=${wpcap_library_dir}/Packet.dll
    -DPCAP_INSTALL_DIR:PATH=<INSTALL_DIR>
	  -DPCAP_INCLUDE_DIR:PATH=<BINARY_DIR>/Include
    -P ${CMAKE_CURRENT_LIST_DIR}/pcap.install.cmake
)

superbuild_add_extra_cmake_args(
  -DPCAP_LIBRARY:FILEPATH=<INSTALL_DIR>/lib/wpcap.lib)

