message("Installing pcap")

file(INSTALL ${PCAP_SHARED_LIBRARY} ${PACKET_SHARED_LIBRARY}
	DESTINATION "${PCAP_INSTALL_DIR}/bin")

file(INSTALL ${PCAP_STATIC_LIBRARY}
	DESTINATION "${PCAP_INSTALL_DIR}/lib")

file(GLOB include_files "${PCAP_INCLUDE_DIR}/*.h")
file(INSTALL ${include_files}
	DESTINATION "${PCAP_INSTALL_DIR}/include")

file(GLOB include_files "${PCAP_INCLUDE_DIR}/pcap/*.h")
file(INSTALL ${include_files}
	DESTINATION "${PCAP_INSTALL_DIR}/include/pcap")

message("Done pcap install")