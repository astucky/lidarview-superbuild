# Stock CI builds test everything possible (platforms will disable modules as
# needed).
set(LIDARVIEW_BUILD_ALL_MODULES ON CACHE BOOL "")
set(BUILD_TESTING ON CACHE BOOL "")

set(ENABLE_lidarview ON CACHE BOOL "")
set(ENABLE_hesaisdk ON CACHE BOOL "")
set(ENABLE_velodynesdk ON CACHE BOOL "")
set(ENABLE_slam ON CACHE BOOL "")
set(ENABLE_pdal ON CACHE BOOL "")
set(ENABLE_pcl ON CACHE BOOL "")

# For python venv
set(ENABLE_paraviewweb ON CACHE BOOL "")

set(USE_SYSTEM_qt5 ON CACHE BOOL "")

file(TO_CMAKE_PATH "$ENV{CI_PROJECT_DIR}/source-lidarview" lidarview_source_dir)

set(lidarview_SOURCE_SELECTION "source" CACHE STRING "")
set(lidarview_SOURCE_DIR "${lidarview_source_dir}" CACHE PATH "")

# Default to Release builds.
if ("$ENV{CMAKE_BUILD_TYPE}" STREQUAL "")
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "")
else ()
  set(CMAKE_BUILD_TYPE "$ENV{CMAKE_BUILD_TYPE}" CACHE STRING "")
endif ()
