$erroractionpreference = "stop"

if ($env:LIDARVIEW_URL) {
    $url = $env:LIDARVIEW_URL
} else {
    $url = "https://gitlab.kitware.com/LidarView/lidarview.git"
}

# figure out which lidarview branch to checkout.
if ($env:LIDARVIEW_BRANCH) {
    $lv_branch = $env:LIDARVIEW_BRANCH
} elseif ($env:CI_COMMIT_TAG) {
    $lv_branch = $env:CI_COMMIT_TAG
} elseif ($env:CI_MERGE_REQUEST_TARGET_BRANCH_NAME) {
    $lv_branch = $env:CI_MERGE_REQUEST_TARGET_BRANCH_NAME
} elseif ($env:CI_COMMIT_BRANCH) {
    $lv_branch = $env:CI_COMMIT_BRANCH
} else {
    $lv_branch = "master"
}

# full clone of lidarview with shallow-submodule. full clone needed so that `git describe` works correctly
$env:GIT_LFS_SKIP_SMUDGE=1
git clone --recursive --shallow-submodules -b "$lv_branch" "$url" "$env:CI_PROJECT_DIR\source-lidarview"

# let's print lidarview version for reference even when the artifacts disappear
git -C "$CI_PROJECT_DIR\source-lidarview" describe
