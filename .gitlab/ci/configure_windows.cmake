include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")

set(CMAKE_C_COMPILER_LAUNCHER "buildcache" CACHE STRING "")
set(CMAKE_CXX_COMPILER_LAUNCHER "buildcache" CACHE STRING "")
set(superbuild_replace_uncacheable_flags ON CACHE BOOL "")
