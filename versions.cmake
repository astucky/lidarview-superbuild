# Please use https links whenever possible because some people
# cannot clone using ssh (git://) due to a firewalled network.
# This maintains the links for all sources used by this superbuild.
# Simply update this file to change the revision.
# One can use different revision on different platforms.
# e.g.
# if (UNIX)
#   ..
# else (APPLE)
#   ..
# endif()

if (WIN32)
  superbuild_set_revision(pcap
    GIT_REPOSITORY http://github.com/patmarion/winpcap.git 
    GIT_TAG master) #At Most 1.5.3 ? #master will likely not change
else()
  superbuild_set_revision(pcap
    GIT_REPOSITORY https://github.com/the-tcpdump-group/libpcap.git
    GIT_TAG libpcap-1.10.3)
endif()

superbuild_set_revision(ceres
  GIT_REPOSITORY https://github.com/ceres-solver/ceres-solver
  GIT_TAG 2.1.0)

superbuild_set_revision(glog
  GIT_REPOSITORY https://github.com/google/glog.git
  GIT_TAG v0.6.0)

superbuild_set_selectable_source(lidarview
  # NOTE: When updating this selection, also update the default version in
  # README.md and the LIDARVIEW_VERSION_DEFAULT variable in CMakeLists.txt.
  SELECT 4.4.0
    GIT_REPOSITORY "https://gitlab.kitware.com/LidarView/lidarview.git"
    GIT_TAG        "4.4.0"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/LidarView/lidarview.git"
    GIT_TAG        "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-lidarview")

superbuild_set_selectable_source(slam
  SELECT 2.0.1
    GIT_REPOSITORY "https://gitlab.kitware.com/keu-computervision/slam.git"
    GIT_TAG        "5eb7dae3966e6a2178d8350a497647814a7c39da"
  SELECT git CUSTOMIZABLE DEFAULT
    GIT_REPOSITORY "https://gitlab.kitware.com/keu-computervision/slam.git"
    GIT_TAG        "master"
  SELECT source CUSTOMIZABLE
    SOURCE_DIR "source-slam")

superbuild_set_revision(pcl
  GIT_REPOSITORY https://gitlab.kitware.com/LidarView/pcl
  GIT_TAG pcl-1.13.1-patched)

superbuild_set_revision(flann
  GIT_REPOSITORY https://gitlab.kitware.com/gabriel.devillers/flann.git
  GIT_TAG 1.9.1-patched) # Cannot update flann to 1.9.2 as it requires PkgConfig

superbuild_set_revision(teaserpp
  GIT_REPOSITORY https://github.com/MIT-SPARK/TEASER-plusplus.git
  GIT_TAG v2.0)

superbuild_set_revision(opencv
  GIT_REPOSITORY https://github.com/opencv/opencv.git
  GIT_TAG 4.7.0)

superbuild_set_revision(nanoflann
  GIT_REPOSITORY https://github.com/jlblancoc/nanoflann.git
  GIT_TAG v1.4.3)

superbuild_set_revision(yaml
  GIT_REPOSITORY https://github.com/jbeder/yaml-cpp.git
  GIT_TAG yaml-cpp-0.7.0)

superbuild_set_revision(darknet
  GIT_REPOSITORY https://github.com/pjreddie/darknet.git
  GIT_TAG master)

superbuild_set_revision(g2o
  GIT_REPOSITORY https://github.com/RainerKuemmerle/g2o.git
  GIT_TAG 20230223_git)

superbuild_set_revision(gtsam
  GIT_REPOSITORY https://gitlab.kitware.com/LidarView/gtsam.git
  GIT_TAG 4.2a9-patched)
